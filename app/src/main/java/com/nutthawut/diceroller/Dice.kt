package com.nutthawut.diceroller

class Dice(private val numSides: Int) { // ตั้งเป็น Private ตัวแปร numSides จะไม่สามารถเข้าถึงจากภายนอกได้

    fun roll(): Int {
        return (1..numSides).random()
    }
}